import java.util.*;

class Solution {
    public int majorityElement(int[] nums) {
        int candidate = nums[0];
        int count = 1;

        for(int i = 1; i < nums.length; i++){
            if(candidate == nums[i]){
                count++;
            } else if(count == 0){
                candidate = nums[i];
                count = 1;
            } else{
                count--;
            }
        }

        count = 0;
        for(int num : nums){
            if(num == candidate){
                count++;
            }
        }
        
        if (count >= nums.length / 2) {
            return candidate;
        } else {
            return Integer.MIN_VALUE;
        }
    }

    public static void main(String[] s){
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the length of the array: ");
        int n = sc.nextInt();

        int nums[] = new int[n];
        for(int i = 0; i < n; i++){
            nums[i] = sc.nextInt();
        }

        Solution solution = new Solution();
        int result = solution.majorityElement(nums);
        if (result != Integer.MIN_VALUE) {
            System.out.println("The majority element is: " + result);
        } else {
            System.out.println("No majority element found.");
        }
    }
}
